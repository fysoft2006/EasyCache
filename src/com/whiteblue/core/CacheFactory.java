package com.whiteblue.core;

import com.whiteblue.cache.CacheInterceptor;
import net.sf.cglib.proxy.Enhancer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 缓存对象工厂方法
 * Created by WhiteBlue on 15/5/6.
 */
public class CacheFactory {

    private CacheFactory() {
    }

    public static <T> T get(Class<T> obj) {

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(obj);

        //初始化日志
        Logger logger = LoggerFactory.getLogger(obj);

        enhancer.setCallback(new CacheInterceptor(logger));

        return (T) enhancer.create();
    }
}
