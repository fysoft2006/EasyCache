package demo;

import com.whiteblue.core.CacheFactory;

/**
 * 简单的demo
 * Created by WhiteBlue on 15/5/18.
 */
public class Test {

    public static void main(String args[]) throws InterruptedException {

        UserDao dao = CacheFactory.get(UserDao.class);
        dao.getUser();
        dao.getUsers();
        dao.deleteUser();
    }
}
